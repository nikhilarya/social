<?php ini_set('display_errors','1'); ?>
<!DOCTYPE html>
<html>
<head>
    <title>Register</title>
</head>
<body>
    <h1>Register Form</h1>
    <form action="./../../controller/register/register_handler.php" method="post">
            <label>UserName:</label>
            <input type="text" name="username" placeholder="Enter Username" ><br />
            <label>Email:</label>
            <input type="email" name="email" placeholder="Enter your email" required><br />
            <label>Password:</label>
            <input type="password" name="pass" placeholder="Your pasword" required><br />
            <label>Confirm Password:</label>
            <input type="password" name="c_pass" placeholder="Confirm Password" required><br />
            Already a Member? <a href="login.php">Go Login</a>
            <br />
            <input type="submit" name="submit" value="Register">
    </form>
</body>
</html>